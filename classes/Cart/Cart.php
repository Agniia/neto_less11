<?php 
namespace Cart;

class Cart
{
	protected $productList;
	protected $totalValue;
	protected $cartCurrency;
	
	function __construct($cartCurrency) {
       $this->productList = [];
       $this->totalValue = 0;
       $this->currency = $cartCurrency;
       print "Конструктор класса Cart\n";
    }	
    
    protected function convertToCartCurrency($product)
    {
    	print 'Как-то приводим стоимость товаров к валюте корзины<br>';
    	$convertedPrice = $product->getPrice();
    	return $convertedPrice;
    }
    
    public function getProductList()
    {
    	return $this->productList;
    }
   
    public function addItemToProductList($product)
    {
    	if($product instanceof \MyProducts\Core\SuperProduct)
    	{
    		$this->productList[] = $product;
    		print $product->getProductName() . ' добавлен в корзину<br>';
    	}
    }
    
    public function removeItemFromProductList($product)
    {
    	if($product instanceof \MyProducts\Core\SuperProduct && !empty($this->productList))
    	{
    		$index = array_search($product, $this->productList);
    		if($index !== false)
    		{
    			print $product->getProductName() . ' удален из корзины<br>';
    			unset($this->productList[$index]);
    		}
    	}
    }
    
     public function  getTotalValue()
     {
     	if(!empty($this->productList))
     	{
     		foreach($this->productList as $item)
     		{
     			   $convertedPrice = $this->convertToCartCurrency($item);
     			   $this->totalValue += $convertedPrice;
     		}
     	}
     	return $this->totalValue;
     }
}