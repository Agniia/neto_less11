<?php 
namespace Order;

class Order
{
	protected $productCart;
	
	function __construct($productCart) {
       print "Конструктор класса Order\n";
       if(empty($productCart->getProductList()))
       {
             print "Нет товаров в корзине. Вы ничего не заказали \n";
             die();
        }
        else
              $this->productCart = $productCart;
    }	
       
    public function printOrderDetails()
    {
        echo '<table>';
        echo '<tr><td>Название товара</td>';
        echo '<td>Название продавца</td>';
        echo '<td>Стоимость товара</td>';
        echo '<td>Валюта цены</td>';
        echo '</tr>';
        foreach($this->productCart->getProductList() as $item)
        {
            echo '<tr>';
            echo '<td>'.$item->getProductName().'</td>';
            echo '<td>'.$item->getProductVendor().'</td>';
            echo '<td>'.$item->getPrice().'</td>';
            echo '<td>'.$item->getProductCurrencyCode().'</td>';
            echo '</tr>';
        }
        echo '</table>';
    }
   
}