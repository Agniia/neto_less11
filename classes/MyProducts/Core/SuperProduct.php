<?php 
namespace MyProducts\Core;

abstract class SuperProduct
{
	protected $productName;
	protected $productPrice;
	protected $productCurrencyCode;
	protected $productVendor;
	
	public function setProductName($productName) {
       $this->productName = $productName;
    }
    public function getProductName() {
       return $this->productName;
    }
	public function setProductVendor($productVendor) {
       $this->productVendor = $productVendor;
    }
    public function getProductVendor() {
       return $this->productVendor;
    }
   	public function setProductCurrencyCode($productCurrencyCode) {
       $this->productCurrencyCode = $productCurrencyCode;
    }
    public function getProductCurrencyCode() {
       return $this->productCurrencyCode;
    }
   
	abstract public function setPrice($productPrice);
	abstract public function getPrice();
}