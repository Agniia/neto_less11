<?php 
namespace MyProducts\Products\Auto;

class Auto extends \MyProducts\Core\SuperProduct
{		
	function __construct($productName, $productVendor, $productPrice, $productCurrencyCode)
	{
       $this->productName = $productName;
       $this->productPrice = $productPrice;
       $this->productVendor = $productVendor;
       $this->productCurrencyCode = $productCurrencyCode;
    }
    
	public function setPrice($productPrice)
	{
		$this->productPrice = $productPrice;
	}
	public function getPrice()
	{
		return $this->productPrice;
	}
}