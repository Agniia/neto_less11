<?php
function myAutoloader($className)
{
   $file = str_replace('\\',DIRECTORY_SEPARATOR,$className);
   $fileName = __DIR__.'/classes'.DIRECTORY_SEPARATOR . $file . '.php'; 
   if(file_exists($fileName))
    {
		include $fileName;
    }
}
spl_autoload_register('myAutoloader');

echo '<pre>';
$auto = new \MyProducts\Products\Auto\Auto('Жигули', 'АВТОВАЗ', 10000,'RUB');
var_dump($auto);

echo '<pre>';
$rubberDuck = new \MyProducts\Products\Duck\RubberDuck('Yello besty', 'Crazy Zoo', 100,'EUR');
var_dump($rubberDuck);

echo '<pre>';
$lcdTV= new \MyProducts\Products\TV\LCDTelevisor('Deja vu', 'Matrix has you', 650,'USD');
var_dump($lcdTV);

echo '<pre>';
$ballPen= new \MyProducts\Products\Pen\BallPen('Super Stilo', 'Wright is your Right', 50,'RUB');
var_dump($ballPen);

echo '<pre>';
$cart= new \Cart\Cart('RUB');
var_dump($cart);
echo '<br>';
$cart->addItemToProductList($auto);
$cart->addItemToProductList($rubberDuck);
$cart->addItemToProductList($lcdTV);
$cart->addItemToProductList($ballPen);
echo '<pre>';
$cart->removeItemFromProductList($lcdTV);
echo '<pre>';
echo $cart->getTotalValue();
echo '<pre>';
$cart->addItemToProductList($ballPen);
echo '<pre>';
$order= new \Order\Order($cart);
echo '<pre>';
$order->printOrderDetails();
